﻿using MVCDemo3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCdemo3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult MoviesList()
        {
            ViewBag.Message = "Your Movies list page.";
            var list = getNameOfFiveMovies();
            return View(list);
        }
        private static List<Movies> getNameOfFiveMovies()
        {
            List<Movies> local = new List<Movies>();

            Movies temp = new Movies();
            temp.name = "Iron Man";
            temp.actor = "Tony Stark";
            
            local.Add(temp);

            temp = new Movies();
            temp.name = "Cap America";
            temp.actor = "Steve Rogers";
            
            local.Add(temp);

            temp = new Movies();
            temp.name = "Hulk";
            temp.actor = "Bruce Banner";

            local.Add(temp);

            temp = new Movies();
            temp.name = "Spider Man";
            temp.actor = "Peter Parker";

            local.Add(temp);

            temp = new Movies();
            temp.name = "Super Man";
            temp.actor = "Kent Clarke";

            local.Add(temp);

            return local;
        }
        public ActionResult ShowList()
        {
            ViewBag.Message = "Your showlist page.";
            var ListOfSuperHeroes = new List<SuperHero>();

            ListOfSuperHeroes = GetFiveSuperHeroes();


            //return View(db.NewsModel.ToList());
            return View(ListOfSuperHeroes);
            return View();
        }
        //this creates a list of super heros. 
        //we will display this in the view
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);
            return tempList;
        }
    }
}