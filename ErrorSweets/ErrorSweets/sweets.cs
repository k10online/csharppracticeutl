﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorSweets
{
    class sweets
    {
        //Open Shop
        private bool ShopOpenOrClose {
            set;
            get;
        }
        private int NumberOfSweets
        {
            set;
            get;
        }

        public sweets()
        {
            this.NumberOfSweets = 50;
            openShop();
        }

        public void openShop()
        {
            ShopOpenOrClose = true;  
        }

        public bool getShopStatus()
        {
            return ShopOpenOrClose;
        }

        public void closeShop()
        {
            ShopOpenOrClose = false;
        }

        //Close Shop

        //Make Sweets
    }
}
