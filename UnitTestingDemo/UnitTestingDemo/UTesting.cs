﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class UTesting
    {
        ////unit test for code reuse.
        //copy paste this code file to build new unit tests quickly.
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void TestSampleOne()
            {
                var actual = true;
                var expected = true;
               
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleTwo()
            {
                var actual = true;
                var expected = false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleThree()
            {
                var inp1 = 13237.85;
                var inp2 = 17897.65;
                
                var expected = 31135.5;
                var actual = Program.sumOfTwoNumbers(inp1,inp2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleFour()
            {
                var inp1 = 13237.85;
                var inp2 = 17897.65;

                var expected = 31135.5;
                var actual = Program.sumOfTwoNumbers(inp1, inp2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleFive()
            {
                var inp1 = 20277.85;
                var inp2 = 23399.76;

                var expected = 43677.61;
                var actual = Program.sumOfTwoNumbers(inp1, inp2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSampleSix()
            {
                SuperHero input = new SuperHero();
                input.age = 30;
                input.brand = "DC";
                input.name = "Batman";
                input.power = "Money";
                List<SuperHero> list = Program.GetFiveSuperHeroes();
                var expected = input;
                var actual = Program.SearchByAge(list, 30);

                var expectedinp = true;
                var actualop = false;
                if(expected.age==actual.age &&
                    expected.name.Equals(actual.name) &&
                    expected.power==actual.power &&
                    expected.brand == actual.brand)
                {
                    actualop = true;
                }
                Assert.AreEqual(expectedinp, actualop);
            }
            [TestMethod]
            public void TestSampleSeven()
            {
                SuperHero input = new SuperHero();
                input.age = 130;
                input.brand = "DC";
                input.name = "Batman";
                input.power = "Money";
                List<SuperHero> list = Program.GetFiveSuperHeroes();
                var expected = input;
                var actual = Program.SearchByAge(list, 30);

                var expectedinp = true;
                var actualop = false;
                if (expected.age == actual.age &&
                    expected.name.Equals(actual.name) &&
                    expected.power == actual.power &&
                    expected.brand == actual.brand)
                {
                    actualop = true;
                }
                Assert.AreEqual(expectedinp, actualop);
            }
        }
    }
}
