﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            var ListSuperHero2 = new List<SuperHero>();

            var tempHero1 = new SuperHero();
            tempHero1.name = "Superman";
            tempHero1.power = "Superpower";
            tempHero1.brand = "DC";
            tempHero1.age = 30;

            var tempHero2 = new SuperHero();
            tempHero2.name = "Wonder Woman";
            tempHero2.power = "Superpower";
            tempHero2.brand = "DC";
            tempHero2.age = 20;
            
            ListSuperHero2.Add(tempHero1);
            ListSuperHero2.Add(tempHero2);

            //printList(ListSuperHero2);
            
            ListOfSuperHeroes = GetFiveSuperHeroes();
            var finalList = new List<SuperHero>();
            finalList.AddRange(ListOfSuperHeroes);
            finalList.AddRange(ListSuperHero2);
            printList(finalList);

            int age = 100;
            SuperHero returnedHero = findByAge(age,finalList);
            Console.WriteLine("-----------Found----------");
            Console.WriteLine("Name " + returnedHero.name);
            Console.WriteLine("Age " + returnedHero.age);
            Console.WriteLine("Power  " + returnedHero.power);
            Console.WriteLine("Brand " + returnedHero.brand);
            Console.WriteLine("------------------");

            String name = "Spiderman34";
            try
            {
                returnedHero = findByName(name, finalList);
                if(returnedHero.Equals(null))
                Console.WriteLine("-----------Found----------");
                Console.WriteLine("Name " + returnedHero.name);
                Console.WriteLine("Age " + returnedHero.age);
                Console.WriteLine("Power  " + returnedHero.power);
                Console.WriteLine("Brand " + returnedHero.brand);
                Console.WriteLine("------------------");
            }catch(Exception e)
            {
                Console.WriteLine("Not found");
            }
            
            
            Console.ReadLine();
            
        }
        private static SuperHero findByName(String name, List<SuperHero> finalList)
        {

            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            var localHero = finalList.Select(x => x).Where(x => x.name == name).FirstOrDefault();

            return localHero;
        }

        private static SuperHero findByAge(int age, List<SuperHero> finalList)
        {
            
            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            var localHero=finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();

            return localHero;
        }

        public static void printList(List<SuperHero> list)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            list=list.OrderByDescending(x => x.age).ToList();

            foreach (SuperHero hero in list){
                //Console.WriteLine(hero.name+" "+hero.brand+" "+hero.power+" "+hero.age);
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + hero.name);
                Console.WriteLine("Age " + hero.age);
                Console.WriteLine("Power  " + hero.power);
                Console.WriteLine("Brand " + hero.brand);
                Console.WriteLine("------------------");
            }
        }

        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();
            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 42;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();
            name = "Spiderman";
            power = "spidey power";
            brand = "Marvel";
            age = 18;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);
            tempHero = new SuperHero();
            name = "Wolverine";
            power = "Claws";
            brand = "Marvel";
            age = 200;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);
            tempHero = new SuperHero();
            name = "Krish";
            power = "Jadoo";
            brand = "India";
            age = 30;
            
            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);
            tempHero = new SuperHero();
            name = "Hulk";
            power = "Angry";
            brand = "Marvel";
            age = 50;
            
            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);
            tempHero = new SuperHero();
            name = "Captain America";
            power = "Vitality";
            brand = "Marvel";
            age = 100;

            tempHero6.name = name;
            tempHero6.power = power;
            tempHero6.brand = brand;
            tempHero6.age = age;

            //add this hero to list. 
            tempList.Add(tempHero6);


            return tempList;
        }
    }
}
