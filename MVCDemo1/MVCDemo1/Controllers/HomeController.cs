﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo1.Controllers
{
    public class SuperHero
    {
        public int age;
        public String name, power, brand;
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Ketan's Contacts...";
            ViewBag.Message2 = "Veeresh's Contacts...";
            ViewBag.Message3 = "hello";
            return View();
        }
        public ActionResult SuperHero()
        {
            List<SuperHero> list = new List<SuperHero>();
            list = GetFiveSuperHeroes();
            //ViewBag.Numberoftimes=list.
            //ViewBag.Message = list.ToList();
            return View(list.ToList());
        }
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();
            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 42;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();
            name = "Spiderman";
            power = "spidey power";
            brand = "Marvel";
            age = 18;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);
            tempHero = new SuperHero();
            name = "Wolverine";
            power = "Claws";
            brand = "Marvel";
            age = 200;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);
            tempHero = new SuperHero();
            name = "Krish";
            power = "Jadoo";
            brand = "India";
            age = 30;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);
            tempHero = new SuperHero();
            name = "Hulk";
            power = "Angry";
            brand = "Marvel";
            age = 50;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);
            tempHero = new SuperHero();
            name = "Captain America";
            power = "Vitality";
            brand = "Marvel";
            age = 100;

            tempHero6.name = name;
            tempHero6.power = power;
            tempHero6.brand = brand;
            tempHero6.age = age;

            //add this hero to list. 
            tempList.Add(tempHero6);


            return tempList;
        }

    }
}