﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SweetsShop2
{
    class Shop
    {
        private int NoOfCustomers,Price,Quantity,TotalRevenue;
        private int Stock, StockTemp, TotalSweetsSold;
        public Shop()
        {
            TotalRevenue = 0;
            TotalSweetsSold = 0;
        }

        public int TakeandReturnInput()
        {
            return Convert.ToInt32(Console.ReadLine());
        }

        public void PrintMessages(String message)
        {
            Console.WriteLine(message);
        }
        public void getCustomerNumber()
        {
            String message = "Enter the number of customers";
            PrintMessages(message);
            NoOfCustomers = TakeandReturnInput();
        }

        public void setStock()
        {
            String message = "Enter the stock of sweets to maintain";
            PrintMessages(message);
            Stock = TakeandReturnInput();
            StockTemp = Stock;
        }

        public void setPriceOfSweet()
        {
            String message = "Enter the price per sweet";
            PrintMessages(message);
            Price = TakeandReturnInput();
        }
        public int SellSweets(int i)
        {
            String message = "Hello Customer " + (i + 1);
            PrintMessages(message);
            message = "Enter the quantity of sweets";
            PrintMessages(message);
            Quantity = TakeandReturnInput();
            if (Quantity > Stock)
            {
                RefillStock();
            }
            message = "The bill was " + Quantity * Price;
            PrintMessages(message);

            Stock -= Quantity;
            TotalSweetsSold += Quantity;

            message = "Total remaining sweets was "+Stock;
            PrintMessages(message);
            message = "-----------------------------------------------";
            PrintMessages(message);
            return Quantity * Price;
        }

        public void RefillStock()
        {
            //Stock = Stock + (StockTemp - Stock);
            Stock = StockTemp;
            String message = "Stock Replenished...";
            PrintMessages(message);
            message = "Stock is now" + Stock;
            PrintMessages(message);
        }

        public void IterateThroughCustomers()
        {
            setStock();
            //must know ho many customers to iterate through
            getCustomerNumber();
            //must set a uniform price per sweet->REQUIREMENT
            setPriceOfSweet();
            //cater throught each customer
            for(int i = 0; i < NoOfCustomers; i++)
            {
                //add the revenue generated to the totalrevenue
                TotalRevenue+=SellSweets(i);
            }
        }
        public void PrintRevenue()
        {
            String message = "Total revenue generated at the end of the day is " + TotalRevenue;
            PrintMessages(message);
            message = "Total sweeets sold was" + TotalSweetsSold;
            PrintMessages(message);
            message = "Total remaining sweets are "+Stock;
            PrintMessages(message);
            message = "Price per sweet was " + Price;
            PrintMessages(message);
        }
    }
}
